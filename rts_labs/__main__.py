from time import sleep

from rts_labs import lib


def main():
    driver = lib.init_chrome()
    lib.search_google(driver, "RTS Labs")
    result_url = lib.get_search_result(driver, 1)
    driver.get(result_url)
    # to prevent chrome from closing once page loads
    sleep(10)
    driver.implicitly_wait(10)
    driver.quit()


if __name__ == "__main__":
    main()
