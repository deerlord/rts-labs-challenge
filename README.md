# RTS Labs Challenge

## Install

```bash
python3 -m venv venv
source venv/bin/activate
pip install poetry
poetry install
python rts_labs
```