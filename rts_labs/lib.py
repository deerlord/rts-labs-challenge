from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager  # type: ignore


def init_chrome() -> webdriver.Chrome:
    """
    Create Chrome driver
    """
    service = Service(executable_path=ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service)
    return driver


def search_google(driver: webdriver.Chrome, value: str) -> None:
    """
    Search google.com for value
    """
    driver.get("http://www.google.com")
    search_box = driver.find_element(By.NAME, "q")
    search_box.send_keys(f"{value}\n")


def get_search_result(driver: webdriver.Chrome, index: int) -> str:
    """
    Find the search result given by index. Must be 1 or greater
    """
    main_block = driver.find_element(By.ID, "rso")
    results = main_block.find_elements(By.CLASS_NAME, "yuRUbf")
    length = len(results)
    if index > length:
        raise ValueError(f"Requested result {index} but only found {length}")
    result = results[index - 1]
    link = result.find_element(By.TAG_NAME, "a")
    return link.get_attribute("href")
